# -*- coding: utf-8 -*-
"""
Created on Mon Dec 26 15:40:34 2016

@author: Ana
"""

algorithmVersion='V1'
numClusters=5
minCustomers=50
PCA_num=15
connectivityGraph=15

usersFile='C:\Users\Ana\Documents\Python Scripts\CustomersSegm\data\user_demographics.csv'
cartsFile='C:\Users\Ana\Documents\Python Scripts\CustomersSegm\data\user_purchase_details.csv'
cartColumns=['userID','week','year','minute','units','store_type','dollars','storeID','upc','trip_count','condition_satisfied']
userColumns=['userID','user_type', 'income', 'family_size', 'race',
            'residence_type', 'county', 'age', 'education', 'occupation',
            'male_age', 'male_education', 'male_occupation',
            'male_working_hours', 'male_smoke', 'female_age',
            'female_education', 'female_occupation', 'female_working_hours',
            'female_smoke', 'number_dogs', 'number_cats', 'children_group_code',
            'marital_status', 'language', 'number_tv', 'number_tv_with_cable',
            'hisp_flag', 'hisp_cat', 'race2', 'race3', 'microwave_owned',
            'device_type', 'zip_code', 'fips_code', 'market_zipcode',
            'market_name', 'ext_fact']

import pandas as pd
from sklearn.cluster import AgglomerativeClustering
from sklearn.neighbors import kneighbors_graph
from sklearn.decomposition import PCA
from datetime import datetime





def createFeatures(data):

    temp=pd.get_dummies(data['user_type'])
    temp=temp.rename(columns = {0:'Card Only',
                  5:'Card and key',
                  6:'Card switch from key',
                  9:'Key switch from card'})
    data=pd.concat([data,temp], axis=1, join_axes=[data.index]) 
    data.drop(['user_type'], axis=1, inplace=True)
    
    temp=pd.get_dummies(data['male_occupation'])
    temp=temp.rename(columns={ 0:'maleOtherOccupation', 
                  1:'maleProfessional',
                  2:'maleManager',
                  3:'maleSales',
                  4:'maleClerical',
                  5:'maleCraftsman',
                  6:'maleOperative', 
                  7:'maleLaborer',
                  8:'maleCleaningFoodHealthService',
                  9:'malePrivateHouseholdWorker', 
                  10:'maleRetired',
                  11:'maleNoSuchHeadOfHousehold',
                  13:'maleNoOccupation' })
    #data=pd.concat([data,temp], axis=1, join_axes=[data.index]) 
    data.drop(['male_occupation'], axis=1, inplace=True)
    
    
    temp=pd.get_dummies(data['female_occupation'])
    temp=temp.rename(columns={ 0:'femaleOtherOccupation', 
                  1:'femaleProfessional',
                  2:'femaleManager',
                  3:'femaleSales',
                  4:'femaleClerical',
                  5:'femaleCraftsman',
                  6:'femaleOperative (machine operator)', 
                  7:'femaleLaborer',
                  8:'femaleCleaningFoodHealthService',
                  9:'femalePrivateHouseholdWorker', 
                  10:'femaleRetired',
                  11:'femaleNoSuchHeadOfHousehold',
                  13:'femaleNoOccupation' })
    #data=pd.concat([data,temp], axis=1, join_axes=[data.index]) 
    data.drop(['female_occupation'], axis=1, inplace=True)
    
    temp=pd.get_dummies(data['male_working_hours'])
    temp=temp.rename(columns={1:'maleNotEmployed', 
                  2:'malePartTime',
                  3:'maleFullTime',  
                  4:'maleRetired',
                  99:'maleWorkingHoursUnknown'})
    data=pd.concat([data,temp], axis=1, join_axes=[data.index]) 
    data.drop(['male_working_hours'], axis=1, inplace=True)
    
    temp=pd.get_dummies(data['female_working_hours'])
    temp=temp.rename(columns={1:'femaleNotEmployed', 
                  2:'femalePartTime',
                  3:'femaleFullTime',  
                  4:'femaleRetired',
                  5:'femaleHomemaker',
                  99:'femaleWorkingHoursUnknown'})
    data=pd.concat([data,temp], axis=1, join_axes=[data.index]) 
    data.drop(['female_working_hours'], axis=1, inplace=True)
    
    
    temp=pd.get_dummies(data['race3'])
    temp=temp.rename(columns={1:'raceWhite',
                  2:'raceBlackAfricanAmerican', 
                  3:'raceHispanic',
                  4:'raceAsian',
                  5:'raceOther',
                  99: 'raceUnkown'})
     
    data=pd.concat([data,temp], axis=1, join_axes=[data.index]) 
    data.drop(['race3'], axis=1, inplace=True)
    
    
    temp=pd.get_dummies(data['marital_status'])
    temp=temp.rename(columns={1:'Single',
                  2:'Married', 
                  3:'Divorced',
                  4:'Widowed', 
                  5:'Separated',
                  99: 'MaritalStatusUnkown'}) 
    data=pd.concat([data,temp], axis=1, join_axes=[data.index]) 
    
    temp=pd.get_dummies(data['residence_type'])
    temp=temp.rename(columns={3:'ResidenceTypeUnknown',
                  1:'ResidenceRented', 
                  2:'ResidenceOwned'}) 
    data=pd.concat([data,temp], axis=1, join_axes=[data.index])
    
    #code the rest of features
    
    temp=data[data['male_age']<3].index
    data['maleAgeYoung']=0
    data.loc[temp,'maleAgeYoung']=1
    data.loc[temp,'male_age']=0
    
    temp=data[data['male_age']>5].index
    data['maleAgeSenior']=0
    data.loc[temp,'maleAgeSenior']=1
    data.loc[temp,'male_age']=0
    
    temp=data[data['male_age']>0].index
    data['maleAgeMiddle']=0
    data.loc[temp,'maleAgeMiddle']=1
    
    
    temp=data[data['female_age']<3].index
    data['femaleAgeYoung']=0
    data.loc[temp,'femaleAgeYoung']=1
    data.loc[temp,'female_age']=0
    
    temp=data[data['female_age']>5].index
    data['femaleAgeSenior']=0
    data.loc[temp,'femaleAgeSenior']=1
    data.loc[temp,'female_age']=0
    
    temp=data[data['female_age']>0].index
    data['femaleAgeMiddle']=0
    data.loc[temp,'femaleAgeMiddle']=1
    
    
    temp=data[data['income']<8].index
    data['lowIncome']=0
    data.loc[temp,'lowIncome']=1
    
    
    temp=data[data['income']>7].index
    data['highIncome']=0
    data.loc[temp,'highIncome']=1
    
    data=data.rename(columns = {'family_size':'bigFamily'})
    data=data.rename(columns = {'number_dogs':'hasDog'})
    data=data.rename(columns = {'number_cats':'hasCats'})
    data=data.rename(columns = {'children_group_code':'manyChildren'})
    data=data.rename(columns = {'male_education':'maleEducated'})
    data=data.rename(columns = {'female_education':'femaleEducated'})
    data=data.rename(columns = {'county':'bigCounty'})
    
    
    data.drop(['bigFamily','income','residence_type','language','marital_status','race','hisp_flag','hisp_cat','race2','microwave_owned','device_type','ext_fact','number_tv','number_tv_with_cable','market_zipcode','fips_code','zip_code','market_name','age','education','occupation','male_age','female_age'], axis=1, inplace=True)
    
    return data
 
def createClusters(data):
    temp=(data - data.mean()) / (data.max() - data.min()) #normalize
    data=temp.fillna(0)
    
    knn_graph = kneighbors_graph(data, connectivityGraph, include_self=False)
    
    model = AgglomerativeClustering(linkage='ward', connectivity=knn_graph , n_clusters=numClusters)
    groups=pd.DataFrame(model.fit_predict(data), index=data.index, columns=['cluster'])
    
    data=pd.concat([data, groups], axis=1, join_axes=[data.index])
    
    centroids=data.groupby(['cluster']).mean()
    
    for c in centroids.columns:
        m=centroids[c].max()
        for i in range(numClusters):
            if centroids.loc[i,c]==m:
                centroids.loc[i,c]=1
            else:
                centroids.loc[i,c]=0
    return data, centroids
    
    
def transformFeatures(dataOrig):
       # PCA
       pca = PCA(n_components=PCA_num)
       data=pd.DataFrame(pca.fit_transform(dataOrig))
       # normalize
       data=(data - data.mean()) / (data.max() - data.min()) 
       data=data.fillna(0)       
       
       # determine for each PCA what are relevant original features
       features=pd.DataFrame(abs(pca.components_) , columns=dataOrig.columns)
       major_feat=features.max(axis=1)
       features=features.T
       
       # and assign name of the most relevant to each PC
       for i in range(len(major_feat)):
           name=features[features[i]==major_feat[i]].index.values[0]
           if name in data.columns:
               features.loc[name,i]=0
               temp=features.loc[:,i].max()
               name=features[features[i]==temp].index.values[0]
               if name in data.columns:
                   data.drop(i, inplace=True, axis=1)
                   major_feat.drop(i)
           data=data.rename(columns={i:name})
       return data    

def formOutput(centroids):
    clusters=pd.DataFrame([], index=range(numClusters), columns=['cluster name','leadingCategories']) 
    
    for i in range(numClusters):
        temp=centroids.loc[i,:]
        temp=temp[temp==1]
        clusters.loc[i,'leadingCategories']=temp.index.values       
        
    return clusters
 


# main code   
   
dataAll=pd.read_csv(usersFile,delimiter=',')
dataAll.columns=userColumns

carts=pd.read_csv(cartsFile)
carts.columns=cartColumns

carts=carts[['userID','storeID']]
carts=carts.groupby(['userID','storeID']).count()
carts=pd.DataFrame(carts.to_records())

dataAll=dataAll.merge(carts, on=['userID'], how='inner')

stores=pd.DataFrame(carts['storeID'].unique(), columns=['store'])

Results=pd.DataFrame([],columns=['customerID','store','cluster','description'])

for s in range(len(stores)):
    data=dataAll[dataAll['storeID']==stores.loc[s,'store']]
    
    if len(data)>minCustomers:
    
       userIDs=data['userID']
       storeIDs=data['storeID']    
    
       data=createFeatures(data)    
       

       
       data.drop(['userID','storeID'], inplace=True, axis=1)
       
       data=transformFeatures(data)
    
       data, centroids = createClusters(data)

       clusters=formOutput(centroids)       
       data['description']=data['cluster'].map(lambda x: clusters.loc[x, 'leadingCategories'])
       data['cluster']=data['cluster'].map(lambda x: 'cluster_'+str(x))
       
       data=data[['cluster','description']]
       
       data['customerID']=userIDs.values
       data['store']=storeIDs.values
       
       Results=Results.append(data)
       
Results['algorithmVersion']=algorithmVersion
Results['timestamp']=str(datetime.now())      

Results.to_csv('Demographics clustering.csv')
