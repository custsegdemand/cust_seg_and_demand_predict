# -*- coding: utf-8 -*-
"""
Created on Fri Dec 23 11:19:05 2016

@author: Ana
"""

#he code assumes that there are user_purchase_details.csv and product_attributes.csv files stored localy. 
#Paths to the files are defined in the first two lines after imports.
#The code will result in saved file 'Cart clusters par shop.csv'
#Adjustable parameters are number PC that feature set is translated to and range in which optimal number of clusters is searched for each store




algorithmVersion='V1'

PCA_num=20

minUsers=50

num_clusters=range(3,6)

numDescFeatures=5 #number of leading features to show in cluster description

cartsFile='C:\Users\Ana\Documents\Python Scripts\CustomersSegm\data\user_purchase_details.csv'
cartColumns=['userID','week','year','minute','units','store_type','dollars','storeID','upc','trip_count','condition_satisfied']

productsFile='C:\Users\Ana\Documents\Python Scripts\CustomersSegm\data\product_attributes.csv'

resultsFile='Cart clusters per shop.csv'
#### above this line are values which you might want in config file


import pandas as pd
import re
from sklearn.decomposition import PCA
from sklearn import metrics
from scipy.cluster.hierarchy import fcluster, linkage
from datetime import datetime

def LoadPrep():
    
    carts=pd.read_csv(cartsFile)   
    carts.columns=cartColumns
    
    
    products=pd.read_csv(productsFile, error_bad_lines=False)
    
    products=products[['upc','item_category']]
    
    products.drop_duplicates(subset=['upc'], inplace=True, keep='first')  
    
    
    products['upc']=products['upc'].map(lambda x: int(x.replace('-','')))
    products['item_category']=products['item_category'].map(lambda x: re.sub(r'.*_', '', x))
    products['item_category']=products['item_category'].map(lambda x: re.sub('diapers', 'diaper', x))
    
    categories=pd.get_dummies(products['item_category'])   
    
    products= pd.concat([products, categories], axis=1, join_axes=[products.index])
    products.drop('item_category', inplace=True, axis=1)
       
    carts=carts[['userID','year','week','minute','upc','storeID']]
    carts=carts.merge(products, on=['upc'], how='inner')
    return carts

def CreateFeatures(carts):
    store_carts=carts[carts['storeID']==stores.loc[s,'store']]
    store_carts.drop(['storeID'],inplace=True, axis=1)
    
    # number of products per category per user per week
    trips=store_carts.groupby( [ 'userID','year', 'week'] ).sum()
    trips=pd.DataFrame(trips.to_records()) 
    
    trips.drop(['upc','minute'], inplace=True, axis=1)
    
    # Average weekly number of products per user per category
    users=trips.groupby( [ 'userID'] ).mean()
    users=pd.DataFrame(users.to_records()) 
    users.drop(['year','week'], inplace=True, axis=1)    
    
    return users

def transformFeatures(users):
       # PCA
       pca = PCA(n_components=PCA_num)
       data=pd.DataFrame(pca.fit_transform(users))
       # normalize
       data=(data - data.mean()) / (data.max() - data.min()) 
       data=data.fillna(0)       
       
       # determine for each PCA what are relevant original features
       features=pd.DataFrame(abs(pca.components_) , columns=users.columns)
       majorFeat=features.max(axis=1)
       features=features.T
       
       # and assign name of the most relevant to each PC
       for i in range(len(majorFeat)):
           name=features[features[i]==majorFeat[i]].index.values[0]
           if name in data.columns:
               features.loc[name,i]=0
               temp=features.loc[:,i].max()
               name=features[features[i]==temp].index.values[0]
               if name in data.columns:
                   data.drop(i, inplace=True, axis=1)
                   majorFeat.drop(i)           
           
           data=data.rename(columns={i:name})
       return data

def clusterCustomers(data):
       # construct clustering tree 
       Z = linkage(data, 'ward')    
       
       m=0
       opt_k=0
       
       # and scan through range of potential number of clusters to find the largest Silhouette Coefficient
       for j in num_clusters:
           labels=pd.Series(fcluster(Z,j,criterion='maxclust'))
           if len(labels.unique())>1:
              SilScore=metrics.silhouette_score(data, labels, metric='euclidean')
              if SilScore>m:
                  opt_k=j
       
       # repeat clustering based in clustering tree cutting for optimal number of clusters           
       labels=pd.Series(fcluster(Z,opt_k,criterion='maxclust'))

       return labels, opt_k
       
def formOutput(data, result):
       centroids=data.groupby(['cluster']).mean()
       centroids=centroids.T
       
       # find categories specific for each cluster (if list of categories is empty, it means that cluster contains customers who buy little of everything) 
       result['categories']=''
       for j in range(1,opt_k+1):
           cl_index=result[result['cluster']==j].index
           cl_desc=str(centroids.sort(j).index.values[:numDescFeatures])
           cl_desc=re.sub("'","" , cl_desc)

           result.loc[cl_index,'categories']=result.loc[cl_index,'categories']+cl_desc  
           
       result['cluster']=result['cluster'].map(lambda x: 'cluster_'+str(x))
       return result
       




# main code
carts=LoadPrep()

stores=pd.DataFrame(carts['storeID'].unique(), columns=['store'])
stores['cust_num']=0

Results=pd.DataFrame([],columns=['userID','storeID','cluster','categories'])

for s in range(len(stores)):

    users=CreateFeatures(carts)

    if len(users)>minUsers:  # for stores with more than 50 customers

       stores.loc[s,'cust_num']=len(users)
    
       userID=users['userID']
       users.drop(['userID'], inplace=True, axis=1)
       
       data=transformFeatures(users)       

        
       labels, opt_k=clusterCustomers(data)
       
       data=pd.concat([data,labels], axis=1, join_axes=[data.index])
       data=data.rename(columns={0:'cluster'})

       # sort out format of output    
       result=pd.DataFrame(userID)
       result['storeID']=stores.loc[s,'store']
       result['cluster']=labels
       
       result=formOutput(data, result)
       
       # append each store results
       Results=Results.append(result) 

Results['algorithmVersion']=algorithmVersion
Results['timestamp']=str(datetime.now())  
          
Results.to_csv(resultsFile)

