# -*- coding: utf-8 -*-
"""
Created on Fri Dec 23 00:19:32 2016

@author: Ana

The code assumes that there is user_purchase_details.csv file stored localy. 
Path to the file is defined in the first line after imports.
The code will result in saved file 'Activity Clusters.csv' which contains clustering acording to activity 
and categorization according to money spent.
The code runs for about 20 minutes on my average i7 machine
"""



algorithmVersion='V1'

PCA_num=10

minUsers=50

num_clusters=5

activityNames=['inactive','low active','medium active','active','very active']
spendersNames=['$','$$','$$$','$$$$','$$$$$'] # keep in mind that if you change the number of clusters, you also have to adjust cut-off values calculations in line 130

cartsFile='C:\Users\Ana\Documents\Python Scripts\CustomersSegm\data\user_purchase_details.csv'
cartColumns=['user_id','week','year','minute','units','store_type','dollars','store_id','upc','trip_count','condition_satisfied']

productsFile='C:\Users\Ana\Documents\Python Scripts\CustomersSegm\data\product_attributes.csv'

resultsFile='Activity per shop.csv'
#### above this line are values which you might want in config file

import pandas as pd
from sklearn.cluster import AgglomerativeClustering
from sklearn.neighbors import kneighbors_graph
from datetime import datetime


def createFeatures(data, users):
        trips=data.groupby( [ 'user_id','year', 'week','minute'] ).sum()
        trips=pd.DataFrame(trips.to_records()) 
        trips=trips.sort(['user_id','year','week','minute'])
        trips.index=range(len(trips))
        
        weeks=trips.groupby(['user_id','week']).count()
        weeks=pd.DataFrame(weeks.to_records())
        
        trips['minutes_since_prev']=0
        
        for i in range(len(trips)-1):
            if trips.loc[i,'user_id']==trips.loc[i+1,'user_id']:
                if trips.loc[i+1,'year']>trips.loc[i,'year']:
                    temp=trips.loc[i+1,'week']+52
                else:
                    temp=trips.loc[i+1,'week']
                trips.loc[i,'minutes_since_prev']=(temp-trips.loc[i,'week']) * 10080 + trips.loc[i+1, 'minute']-trips.loc[i,'minute']

        
        users['dollars_mean']=0
        users['dollars_std']=0
        users['numTrips_mean']=0
        users['numTrips_std']=0
        users['recency_mean']=0
        users['recency_std']=0
        
        for i in range(len(users)):
            temp=trips[trips['user_id']==users.loc[i,'user_id']]
            users.loc[i,'dollars_mean']=temp['dollars'].mean()
            users.loc[i,'dollars_std']=temp['dollars'].std()
            
            users.loc[i,'recency_mean']=temp['minutes_since_prev'].mean()
            users.loc[i,'recency_std']=temp['minutes_since_prev'].std()
            
            temp=weeks[weeks['user_id']==users.loc[i,'user_id']]
            users.loc[i,'numTrips_mean']=temp['dollars'].mean() # dollars are actually count of aggregated fields
            users.loc[i,'numTrips_std']=temp['dollars'].std()
            
        users=users.fillna(0)    
        return users
        

def createClusters(users):
    
        usersNorm=(users - users.mean()) / (users.max() - users.min()) #normalize
        usersNorm=usersNorm.fillna(0)
        usersNorm=usersNorm[['numTrips_mean','recency_mean']]
        
        knn_graph = kneighbors_graph(usersNorm, 10, include_self=False)

        model = AgglomerativeClustering(linkage='ward', connectivity=knn_graph , n_clusters=num_clusters)
        groups=pd.DataFrame(model.fit_predict(usersNorm), columns=['cluster'])
        
        usersNorm=pd.concat([usersNorm, groups], axis=1, join_axes=[usersNorm.index])

        centroids=usersNorm.groupby(['cluster']).mean()
    
        centroids['sum']=centroids.loc[:,'numTrips_mean'] + centroids.loc[:,'recency_mean']
        centroids.sort('sum', inplace=True)
        centroids['activity']=activityNames
        
        return centroids, groups     
        
        
def formOutput(users, centroids):
    
        users['activity']=users['activity'].map(lambda x: centroids.loc[x,'activity'])
       
        dollars_M=users['dollars_mean'].mean()
        dollars_V=users['dollars_mean'].std()
        
        users['dollar spent']=0
        
        # catgeries of "spenders" are determined based on mean and std of all customers
        categories=pd.DataFrame({'value' : [dollars_M-2*dollars_V, dollars_M-1*dollars_V, dollars_M, dollars_M+dollars_V, dollars_M+2*dollars_V],
                          'category' : spendersNames})
                          
        for i in range(len(categories)):
            ind=users[users['dollars_mean']>categories.loc[i,'value']].index
            cat=pd.Series(categories.loc[i, 'category'], index=ind)
            users.loc[ind,'dollar spent']=cat      
            
        return users    




# main code

dataAll=pd.read_csv(cartsFile)


dataAll.columns=cartColumns
dataAll=dataAll[['user_id','year','week','minute','dollars','store_id']]


stores=pd.DataFrame(dataAll['store_id'].unique(), columns=['store'])

Results=pd.DataFrame([])

for s in range(len(stores)):
    print s

    data=dataAll[dataAll['store_id']==stores.loc[s,'store']]
    data.drop(['store_id'],inplace=True, axis=1)
    
    users=pd.DataFrame(data['user_id'].unique(), columns=['user_id'])
    users['store_id']=stores.loc[s,'store']
    
    if len(users)>minUsers:
    
       users=createFeatures(data, users)
       
       centroids, groups = createClusters(users)
       
       users['activity']=groups
     
       users=formOutput(users, centroids)
     
       Results=Results.append(users)     


Results['algorithmVersion']=algorithmVersion
Results['timestamp']=str(datetime.now())      
       
Results.to_csv(resultsFile)






      

