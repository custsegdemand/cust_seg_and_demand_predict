import ConfigParser
#====================================
config = ConfigParser.RawConfigParser()
config.read('conf.properties')

self.__HOST = config.get('DatabaseSection', 'database.host’);
self.__PORT = config.get('DatabaseSection', 'database.port’);
self.__USER = config.get('DatabaseSection', 'database.user’);
self.__PASSWORD = config.get('DatabaseSection', 'database.password’);
self.__DATABASE = config.get('DatabaseSection', 'database.databasename’);

self.__DATABASE = config.get('CartClusteringSection', 'carts’);
self.__DATABASE = config.get('CartClusteringSection', 'products’);
self.__DATABASE = config.get('CartClusteringSection', 'PCAnum’);
self.__DATABASE = config.get('CartClusteringSection', 'numCLusters’);
self.__DATABASE = config.get('CartClusteringSection', 'cartColumns’);
#====================================
