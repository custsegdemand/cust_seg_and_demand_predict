# README 

The requirements and the approach are communicated in this md file. Please read the following and build your code accordingly and pay attention to the following and read carefully: 

 - If you need to communicate with any of the project owners if you have any questions you can reach us immeditaly on slack channels.
 - We prefer if you assign yourself tasks on our project managenet tool called Taiga which is very similar to Jira. This way we can know about your plan and progress, and so we can advise you on priorities or approaches to make your life easier. 
 - We will have a sprint meeting for 30 to 60 minutes every 2 weeks so that you can reflect on what you have done, show us any demos if any, and discuss approaches or milestones. Ofcourse we will be available whenever you need us just ping us on slack and we can reach out to you as soon as possible.  
 - You are free to make your own work schedule but by the end of each work session, you are required to push your code everytime you have done some coding, that way we can have a backup and we can monitor your activity and do code reviews. 
 - Your repository is totally manged by you feel free to work whichever way you please, if you want to create your own branches thats ok or if you want to keep pushing in the master branch thats ok as well. However, you are requied to write us a readme.md file to describe how to run your code when it is ready to run, and describe the colomns of the output tables in redshift. 
 - To access our posgress SQL-redshift for the data please use the following credintials: 
 	Host: "retail-analytics.cmrxcu1tepas.us-west-2.redshift.amazonaws.com", port: "5439", DB Name: "iridataset", username: "oliver" password: "qnTT5+9*45eeW!jodP"	
 - To access S3 use the following keys, Access Key: "AKIAI42NJJWDBJXSWOEA", Key Pass: "s5i9cBGNixjjOl0mY5ijmQlwcjylW3lLK+PRMQMs"
 - You encourage you to use S3 to unload your exhaustive huge query into redshift if the size of the data is large, please refer to the DB.py file for an example and read the comments. 
 - Please note that your peers will be using the same access keys and redshift passwords so please do not delete any tables unless you let us know what you exactly want to delete. Unless you want to delete a table you have personally created yourself in case something went wrong then sure you can go ahead. 
 - The delivrables of the project is pretty much a bunch of sql tables as your code writes these tables. Please create your own schema before you create the tables, DO NOT add data or create tables in the public schema.
 - Please create a seperate schema for each usecase and call it for example "customer_segmentation_output"
 - Please add a timestamp column to your output tables. 
 

Wish you the best of luck and remember don't think of us as if we are your boss, baisically we are here to help you achieve your best and learn new things from each other :)   
 
# Customer Segmentation 
 
## General Requirements and Objectives:

The requirement is to segment the customers by different attributes to gain knowledge in the eyes and point of view of a retailer. The objective is to understand the consumer and effectively use this information to build a solid relationship with the consume. This is critical to our research to dissect their analysis based on their basic understanding of the customers behaviour given the data in hand.  Consumer products analytics focuses on understanding the consumption and purchase preferences, behaviours, and patterns of consumers, by applying advanced analytics on both existing and emerging internal and external data. These insights can be useful in designing and delivering differentiated brand experiences through both improved new-product innovation and the execution of a seamless, compelling consumer experience throughout the consumer journey across all channels, including e-commerce (direct to consumer and indirect by way of retailer e-commerce channel), digital, mobile, social and in-store. 

Success here is measured in the ability to understand changing lifestyle needs, and influence consumer behaviour in real-time to drive dramatically higher return on marketing spend, improve the success of new products and, ultimately, increase sales and market share. 

## What the Stake-holder needs ? 

Deep consumer insight to understand each of your consumers as an individual : A new way to describe what you need is something called “brand enthusiasm,” which includes information on who your consumers are, where they live and their demographics, interests, activities, attributes and preferences, and shopping behaviour. However, brand enthusiasm goes far beyond traditional demographics analysis to encompass consumer engagement with the brand experience. Using all this information to properly analyze and segment consumers will be the foundation to drive compelling messages and targeted campaigns. 
To gain understanding show that it is possible that the marketing campaigns can no longer be exercises in broadcasting. And it can be more strategic and planned to reach individual consumers with personalized messages and offers based on history, behavioural drivers and current lifestyle needs to drive shoppers to take the desired action. 

## Datasets to Use:

- user_purchase_details
- user_demographics
- product_attributes

## Approach to follow 

You are required to create 3 types of clustering models. We want to show clustering based on:
1. the spending amounts and frequency of shopping
2. the shopping carts and the items associated together based on the items attributes
3. the user demographics

Each of these customers is labeled with these clustering technique. 

1. Clustering based on spending and activity:

- For each user in the user_purchases_details table, aggregate the sums of the total dollars spent to group them by trip (each time a user goes shopping). This can be done by grouping by minute and dollars spent. 
- For each user add an additional column that describes the how recent the trip was made; this is basically the difference in minutes of a given trip with the previous trip ( i.e. minutes(t)-minutes(t-1)  ), where minutes(t) represents the current trip.  
- Calculate the mean and standard deviation of total dollars aggregated per trip. 
- Calculate the mean and standard deviation of number of trips made in a week. 
- Calculate the mean and standard deviation of the recency (i.e. how recently) of the trips. 
- Use the trips per week and recency mentioned features to create clusters of users that describes them as (very active, active, medium active, low active, inactive)
- Use only the dollars aggregated feature to categorize (not clustering) users by dollar spent category to describe them as ($$$$$, $$$$, $$$, $$, $) 

2. Clustering based on shopping carts:

- For each user’s trip, aggregate (group by) the items purchased for each item/category and attributes (some of them to be discussed later), keep the weeks column to account for seasonality. 
- Cluster based on these features using hierarchal clustering. 
- Explain each cluster based on similarity of purchases and think of a label for each cluster. 

3. Clustering based on user demographics: 

- Use the user_demographics table data to cluster the clients using hierarchal clustering techniques. 
- Explore methods to summarize each of the clusters and come up with labels. You can use PCA to explain these clusters. 
- Create a table in redshift and make it write its output (make sure you use time stamp).

Note: you can use PCA to either reduce the number of columns features, or you can use PCA to explain the output of the clusters by knowing the features that best correlates with the principle components. The data set is huge so you have to use spark(see these examples http://spark.apache.org/docs/latest/ml-classification-regression.html)

## Useful links: 

- https://www.quora.com/How-do-I-use-clustering-to-segment-customers 
- https://pkghosh.wordpress.com/2016/07/30/customer-segmentation-based-on-online-behavior-using-scikitlearn/
- http://www.kdnuggets.com/2014/10/supermarket-customers-segmentation-using-self-organizing-mapping.html 
- https://gist.github.com/hrbrmstr/bf238b72a23bf78eeef8 
- https://www.youtube.com/watch?v=hGdqlM3OauQ&feature=youtu.be (customer segmentation using spark)

# Products Associations 

## scope: 

Basically we need to find association of the product (a product is a vendor, item) with all other products that was bought in each store separately (store by store).  

### Data set used: 
- Customers purchase data (only)
- Product outputs to describe the data 

## How to do it ? 

- Step 1: From the dataset for each store create a shopping cart table that represents the trips of all users. Each shopping trip cart should be represented in one row and multiple of columns. You can ignore the trips that has more than say 50 or 60 items (50 to 60 colomns) in one trip (make this a variable we can play with it later on to check results and optimizations).
- Step 2: Run association models of confidence between 0.6 and higher and support of 0.3 (again make this as a variable) 
- Step 3: Filter out the results that doesn’t make sense: This means some of the shopping cart trips (the rows) may have columns with nan because of the shopping  sizes differed from one trip to the other. In order to the results to make sense You are required to filter out the associations that has nans. 

For example: if one associations looks like this Nescafe, milk, nan ==> milo then after cleaning the output it should look like this Nescafe, milk, milo. If the output looks like this Nescafe, nan, nan ==> milo then after cleaning it should like this Nescafe, milo. If the output looks like this Nescafe, milk, coffeemate ==> milo then the output should looks like this Nescafe, milk, coffeemate, milo. 

- Step 4: Since the output will be only a bunch of product numbers (item-vendor number) we need to understand what this product is. So please these numbers (item vendor numbers) but please translate these numbers to product attributes that makes sense. for example(Nestle - Nescafe, Crest - white, Nestle - coffee mate …etc) Size of the product is not really essential. 

# Demand Prediction 

## General Requirements and Objective: 

You need to integrate data gathered from the entire value chain to better anticipate and meet market demand across multiple channels. You need to apply multi-echelon planning to optimize inventory levels, to fulfil more perfect orders and to effectively weigh the trade-offs between order fulfilment and service levels. 

With consumers now expecting to have complete flexibility in how and when they buy, receive, order and impact product decisions, supply networks are changing dramatically. Supply chain leaders can no longer rely on a standard set of simple order- ow methods for moving products from source to consumer. Front-office and back-office processes must be connected to enable true “anytime, anywhere” commerce. 

Supply chain leaders are looking to greatly improve areas of their network, such as deriving improved analytics from the various disparate information sources available, improving sales and operations planning with cross-organizational synchronization, driving down safety stock and better managing inventory while reducing out-of-stocks, improving service levels while reducing transportation costs and improving product traceability and safety— to name a few. 
Those consumer products companies that can properly use demand and supply signals to improve supply network visibility and synchronization will dramatically improve revenue, reduce operating costs and improve working capital. 

## What the Stalk-holder needs ? 

- How can an organization to be aligned to achieve common goals and provide operational and financial visibility ?
The shift from individual islands of inventory planning to enterprise inventory planning forces organizations to adapt processes and technology to accommodate multi-echelon inventory planning. A global, multi-echelon inventory optimization process strategically positions raw materials, work in progress and finished goods inventory across the supply chain to improve inventory turns, improve service levels, free working capital and increase cash flow.

- How can the supply chain networks and the transportation be optimized ?  
Manufacturers need to minimize the number of trucks used, and the total distance traveled, through effective vehicle routing and customer clustering capabilities, in addition to optimizing the location, size and distribution of manufacturing assets. They need to effectively manage inbound and outbound logistics and warehousing, and quantify the impact of their actions on energy use and carbon emissions. This new optimized network must balance agility with cost. 

## Dataset to Use:

- weather data: https://pypi.python.org/pypi/get-weather-data 
- economic data: https://www.quandl.com/data/FRED-Federal-Reserve-Economic-Data?keyword= 
- External Events: http://api.eventful.com/docs/events/search
- store_data
- store_demographics
- product_attributes

## Approach to follow 

You need to build a regression model and an association model to understand the factors of demands and to estimate the demand for each item. The weather data, economic data, and local events data should be incorporated with the usage data (store_data, store_demographics, product_attributes) for better estimation [ibm]. You need to make sure that the model has fitted perfectly because you have a variety of data. The key success to this task is the correct data to incorporate to your regression model to calculate the price elasticity so you may need to run person correlation between units sold and each and every feature and keep only the columns that are highly correlated between the units sold. Pick the columns that achieved correlation score > 0.1 or < -0.1. 


- using the weather API get the average, standard deviation, min, and max temperature for the week of the designated area of the store for each store in store_data. 
- for each area of the store location (city and county) extract the GDP (Gross Domestic Product), CPI (Consumer Price Index of food), (Producer Price Index), employment rate, and housing prices. All of these can directly affect the consumption of goods so they might be useful for the regression model. To get these data you should be able to find it from quandl.com, search the data bases for each market (county/state) and call the Python or R API to get the required dataset. You can follow the API example on their website or just manually download the data for each location its not a lot. Make sure you get week by week data (daily is fine too) for the years of 2011 2012. 
- Use the product attributes as features in the dataset as well. 
- Extract local business events such as finance, economics, political events as well using the [events_api] , you need to create a random API key to extract the data once. You can use the [events_search] to search for local events by city, date, category, and geolocation. you will have to use the geolocations specified to the store for the years 2011 and 2012. As for events category use [events_category] to know the types and ids of events to use in your event search. Think of events that can directly affect grocery prices such as financial events, sports, economic events or news. 
- Join and merge all these data together in one table to build your regression model.  
- Build a multivariate regression model and make sure it is not overfitting or under fitting. 
- Evaluate the mean square error between the test and training set and try to make it as low as possible
- Using Apriori Algorithm build an association model that finds the associations of demand. You can convert demand into ranges. i.e. 1-10, 10-15, 15-20, 20-50 … etc. 
- Use Spark to make your life easier and faster.  
- Create a table in redshift and make it write its output (make sure you use time stamp). 

## Useful links:

- [ibm] https://www.ibm.com/developerworks/community/blogs/jfp/entry/price_optimization?lang=en
- [elasticity r] http://www.salemmarafi.com/code/price-elasticity-with-r/ 
- [elasticity python] https://github.com/andrewwowens/PED_Statsmodels
- https://www.analyticsvidhya.com/blog/2016/07/solving-case-study-optimize-products-price-online-vendor-level-hard/ 
- [ideal_price1] http://www.dummies.com/education/economics/how-to-determine-the-ideal-price-with-price-elasticity-of-demand/ 
- [ideal_price2] http://rstudio-pubs-static.s3.amazonaws.com/14661_54be2bb6453f4ceb9452c284b6c6e11e.html 
- [events_api] https://api.eventful.com/libs/python/ 
- [events_serarch] http://api.eventful.com/docs/events/search 
- [events_category] http://api.eventful.com/docs/categories/list  
- https://rpubs.com/KakasA09/FARCon082015